//
//  ViewSimple.m
//  PianoTile
//
//  Created by Abhijit Fulsagar on 3/23/17.
//  Copyright © 2017 Abhijit Fulsagar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ViewSimple : UIViewController
{
    NSString *scoretext;
    int color1,color2,color3,color4,count;
    float r;
    float location;
    double label1x,label1y,label2x,label2y,label3x,label3y,label4x,label4y;
    CGRect screenRect;
    CGSize screenSize ;
    CGFloat screenWidth;
    CGFloat screenHeight;
    CGPoint startpoint,endpoint;
    UITapGestureRecognizer *taprecognizer;
    UIAlertView *alert;
    NSTimer *timer1;
    NSTimer *timer2;
    UIView *line;
    
}

@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;
@property (weak, nonatomic) IBOutlet UILabel *label3;
@property (weak, nonatomic) IBOutlet UILabel *label4;
@property (weak, nonatomic) IBOutlet UILabel *score;
@end

@implementation ViewSimple
- (void)viewDidLoad {
    [super viewDidLoad];
    screenRect = [[UIScreen mainScreen] bounds];
    screenSize = screenRect.size;
    screenWidth = screenSize.width;
    screenHeight = screenSize.height;
    color1=color2=color3=color4=1;
    count=0;
    //adding touch recognizer
    taprecognizer=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchtile:)];
    taprecognizer.numberOfTapsRequired=1;
    [self.view addGestureRecognizer:taprecognizer];
    
    scoretext=[NSString stringWithFormat:@"%d",0];
    _score.text=scoretext;
    
    line = [[UIView alloc] initWithFrame:CGRectMake(92,10, 1.0, 700.0)];
    line.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:line];

    line = [[UIView alloc] initWithFrame:CGRectMake(184,10, 1.0, 700.0)];
    line.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:line];

    line = [[UIView alloc] initWithFrame:CGRectMake(278,10, 1.0, 700.0)];
    line.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:line];

    [self start];
}

-(void)touchtile:(UITapGestureRecognizer *) sender
{
    CGPoint currentpoint=[taprecognizer locationInView:self.view];
    if(currentpoint.x>=label1x && currentpoint.x<=(label1x+89) && currentpoint.y>=label1y && currentpoint.y<=(label1y+150) && label1y<=screenHeight)
    {
        _label1.backgroundColor=[UIColor whiteColor];
        [self updatescore];
        color1=0;
    }
    else if(currentpoint.x>=label2x && currentpoint.x<=(label2x+89) && currentpoint.y>=label2y && currentpoint.y<=(label2y+150) && label2y<=screenHeight)
    {
        _label2.backgroundColor=[UIColor whiteColor];
        [self updatescore];
        color2=0;
    }
    else if(currentpoint.x>=label3x && currentpoint.x<=(label3x+89) && currentpoint.y>=label3y && currentpoint.y<=(label3y+150) && label3y<=screenHeight)
    {
        _label3.backgroundColor=[UIColor whiteColor];
        [self updatescore];
        color3=0;
    }
    else if(currentpoint.x>=label4x && currentpoint.x<=(label4x+89) && currentpoint.y>=label4y && currentpoint.y<=(label4y+150) && label4y<=screenHeight)
    {
        _label4.backgroundColor=[UIColor whiteColor];
        [self updatescore];
        color4=0;
    }
    else
    {
        [self endgame];
    }
}
-(void)updatescore
{
    scoretext=_score.text;
    count=[scoretext integerValue];
    count=count+1;
    scoretext=[NSString stringWithFormat:@"%d",count];
    _score.text=scoretext;
}
-(void)endgame
{
    alert = [[UIAlertView alloc] initWithTitle:@"END GAME"
                                       message:@"You Lose"
                                      delegate:self
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
    [alert show];
    NSLog(@"You lose");
   [timer1 invalidate];
    [timer2 invalidate];
    //sleep(10);
}
-(void)start
{
    timer1=[NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(movingtile:) userInfo:NULL repeats:YES ];
    //for getting current position of every label
     timer2=[NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(labelposition:) userInfo:NULL repeats:YES ];
}
-(void)movingtile:timer
{
    r=arc4random_uniform(5);
    [UIView animateWithDuration:0.5
                     animations:^{
                         startpoint.x=_label1.center.x;
                         startpoint.y=_label1.center.y;
                         if(startpoint.y>(screenHeight+50.0) && color1==0)
                         {
                             startpoint.y=30.0-(r*100);
                             _label1.center=startpoint;
                            _label1.text=@"";
                             _label1.backgroundColor=[UIColor blackColor];
                              color1=1;
                         }
                         else if(label1y>=(screenHeight)&& color1==1 )
                         {
                             [self endgame];
                            
                         }
                         startpoint.y=startpoint.y+40.0;
                         _label1.center=startpoint;
                         
                         startpoint.x=_label2.center.x;
                         startpoint.y=_label2.center.y;
                         if(startpoint.y>(screenHeight+50.0)&& color2==0)
                         {
                             startpoint.y=30.0-(r*100);
                             _label2.center=startpoint;
                            _label2.backgroundColor=[UIColor blackColor];
                             color2=1;
                         }
                         else if(label2y>=(screenHeight) )
                         {
                             [self endgame];
                         }
                         startpoint.y=startpoint.y+40.0;
                         _label2.center=startpoint;

                         
                         startpoint.x=_label3.center.x;
                         startpoint.y=_label3.center.y;
                         if(startpoint.y>(screenHeight+50.0) && color3==0)
                         {
                             startpoint.y=30.0-(r*100);
                             _label3.center=startpoint;
                            _label3.backgroundColor=[UIColor blackColor];
                             color3=1;
                         }
                         else if(label3y>=(screenHeight) && color3==1)
                         {
                             [self endgame];
                         }

                         startpoint.y=startpoint.y+40.0;
                         _label3.center=startpoint;

                         startpoint.x=_label4.center.x;
                         startpoint.y=_label4.center.y;
                         if(startpoint.y>(screenHeight+50.0)&& color4==0)
                         {
                             startpoint.y=30.0-(r*100);
                             _label4.center=startpoint;
                            _label4.backgroundColor=[UIColor blackColor];
                             color4=1;
                         }
                         else if(label4y>=(screenHeight) && color4==1)
                         {
                             [self endgame];
                         }

                         startpoint.y=startpoint.y+40.0;
                         _label4.center=startpoint;

                     }//animation ends here
                     completion:^(BOOL finished){
                     }// completion ends here
     ];//uiview ends here
}

-(void)labelposition:timer
{
    label1x=_label1.frame.origin.x;
    label1y=_label1.frame.origin.y;
    label2x=_label2.frame.origin.x;
    label2y=_label2.frame.origin.y;
    label3x=_label3.frame.origin.x;
    label3y=_label3.frame.origin.y;
    label4x=_label4.frame.origin.x;
    label4y=_label4.frame.origin.y;
}
@end
