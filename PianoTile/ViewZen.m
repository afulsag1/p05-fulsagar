//
//  ViewZen.m
//  PianoTile
//
//  Created by Abhijit Fulsagar on 3/26/17.
//  Copyright © 2017 Abhijit Fulsagar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AppDelegate.h"

# import <AVFoundation/AVFoundation.h>

@interface ViewZen : UIViewController
{
    NSString *music;
    AVAudioPlayer *audio;

}
@property (weak, nonatomic) IBOutlet UISwitch *switch1;
@property (weak, nonatomic) IBOutlet UISwitch *switch2;
@property (weak, nonatomic) IBOutlet UISwitch *switch3;
@property (weak, nonatomic) IBOutlet UISwitch *switch4;

@end

@implementation ViewZen
-(void)viewDidLoad
{
    NSLog(@"In Settings");
}
- (IBAction)change1:(id)sender {
    if(_switch1.on)
    {
        music=[[NSBundle mainBundle]pathForResource:@"music" ofType:@"mp3"];
        audio=[[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:music] error:NULL];
        audio.delegate=self;
        audio.numberOfLoops=-1;
        [audio play];
        
    }
    else
        [audio stop];


}
- (IBAction)change2:(id)sender {
    if(_switch2.on)
    {

        [audio stop];
        music=[[NSBundle mainBundle]pathForResource:@"music2" ofType:@"mp3"];
        audio=[[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:music] error:NULL];
        audio.delegate=self;
        audio.numberOfLoops=-1;
        [audio play];
    }
    else
        [audio stop];
}
- (IBAction)change3:(id)sender {
    if(_switch3.on)
    {
        [audio stop];
        music=[[NSBundle mainBundle]pathForResource:@"music3" ofType:@"mp3"];
        audio=[[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:music] error:NULL];
        audio.delegate=self;
        audio.numberOfLoops=-1;
        [audio play];
    }
    else
        [audio stop];

}
- (IBAction)change4:(id)sender {
    if(_switch4.on)
    {
        [audio stop];
        music=[[NSBundle mainBundle]pathForResource:@"music4" ofType:@"mp3"];
        audio=[[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:music] error:NULL];
        audio.delegate=self;
        audio.numberOfLoops=-1;
        [audio play];
    }
    else
        [audio stop];

}
@end
