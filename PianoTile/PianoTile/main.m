//
//  main.m
//  PianoTile
//
//  Created by Abhijit Fulsagar on 3/23/17.
//  Copyright © 2017 Abhijit Fulsagar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
