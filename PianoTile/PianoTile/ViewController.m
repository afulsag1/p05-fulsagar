//
//  ViewController.m
//  PianoTile
//
//  Created by Abhijit Fulsagar on 3/23/17.
//  Copyright © 2017 Abhijit Fulsagar. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIButton *simple;
@property (weak, nonatomic) IBOutlet UIButton *arcade;
@property (weak, nonatomic) IBOutlet UIButton *bomb;
@property (weak, nonatomic) IBOutlet UIButton *settings;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}
- (IBAction)exit:(id)sender {
    exit(0);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
